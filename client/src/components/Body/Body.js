import React from "react";
import "./Body.css";

function Body() {
    return (
        <div>
            <div className="body-video">
                <video src="video.mp4" autoPlay loop muted></video>
            </div>
            <div className="empty-space1"></div>
        </div>
    );
}
export default Body;