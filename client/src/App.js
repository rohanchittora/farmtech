import "./App.css";
import React from "react";
import {
    BrowserRouter as Router,
    Routes,
    Route,
} from "react-router-dom";
import Home from "./pages/home/Home";
import Crop_Recommendation from "./pages/crop-recommendation/Crop_Recommendation";
import Crop_Disease from "./pages/crop-disease-detection/Crop_Disease";
import Fertilizer from "./pages/fertilizer-recommendation/Fertilizer";
import Irrigation from "./pages/irrigation-requirement/Irrigation";

function App() {
    return (
        <div className="App">
            <Router>
                <Routes>
                    <Route exact path='/' element={< Home />}></Route>
                    <Route
                        exact
                        path="/services/crop-recommendation"
                        element={<Crop_Recommendation/>}
                    />
                    <Route
                        exact
                        path="/services/fertilizer-recommendation"
                        element={<Fertilizer/>}
                    />
                    <Route
                        exact
                        path="/services/irrigation-requirement"
                        element={<Irrigation/>}
                    />
                    <Route
                        exact
                        path="/services/crop-disease-detection"
                        element={<Crop_Disease/>}
                    />
                </Routes>
            </Router>
        </div>
    );
}

export default App;