import React from "react";
import Navbar from "../../components/Navbar/Navbar";
import Body from "../../components/Body/Body";
import CardItem from "../../components/Card/CardItem";
function Home() {
    return (
        <div className="Home">
            <Body/>
            <CardItem/>
            <Navbar/>
        </div>
    );
}

export default Home;