import React, { useState } from "react";
import { Link } from "react-router-dom";
import "./Navbar.css";

function Navbar() {
    const [click, setClick] = useState(true);
    const handleClick = () => setClick(!click);
    const blockFormula = `\\int_{agriculture}^{services} (economy) dIndia  =  superpower`;
    return (
        <nav className="navbar">
            <div className="navbar-container">
                <Link to="/home" className="nav-logo">
                <img src="/logo.png" alt="logo" width="60px" height="60px" />
                    </Link>
                </div>


            <div className="home-page-list">
        <span className="home-icon">

            <Link to="/home" className="nav-home">
            Home
          </Link>

        </span>
                <span className="service-icon" onClick={handleClick}>

            <Link>
            Services{" "}
                <i className={click ? "fas fa-sort-down" : "fas fa-sort-up"} />
          </Link>

        </span>
            </div>

        </nav>
    );
}
export default Navbar;