import React, {useState, useEffect} from "react";
import axios from "axios";
import "./Crop_Recommendation.css";
import base_url from "../../service/api";
import {ToastContainer, toast} from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
import {Box, style, styled} from "@mui/system";
import Typography from '@mui/material/Typography';
import {
    FormControl,
    InputLabel,
    Input,
    Select,
    MenuItem,
    Button, Modal,
} from "@mui/material";
import FormGroup from "@mui/material/FormGroup";


const useStyle = styled({
    container: {
        width: "30%",
        margin: "0 0 0 35%",
        "&>*": {
            marginTop: 20,
        },
    },
});

const initialValue = {
    name: "",
    country: "",
    adhaarId: "",
    nitrogen: "",
    phosphorus: "",
    potassium: "",
    temperature: "",
    rainfall: "",
    humidity: "",
    pH: "",
};

export default function Crop_Recommendation() {
    const onValueChange = (e) => {
        setDetails({...detail, [e.target.name]: e.target.value});
    };
    const classes = useStyle();
    const [detail, setDetails] = useState(initialValue);

    const submitData = (e) => {
        console.log(detail);
        postDataToServer(detail);
        e.preventDefault();
        // setDetails({...detail, [e.target.name]: initialValue});
    }

    const showToastSuccessMessage = () => {
        toast.success('Data Submitted Successfully !', {
            position: toast.POSITION.TOP_RIGHT
        });
    };

    function showResultToast(response) {
        toast(response.data.result, {position: toast.POSITION.BOTTOM_CENTER});
    }

    const showToastErrorMessage = () => {
        toast.error('Error submitting data!!', {
            position: toast.POSITION.TOP_RIGHT
        });
    };

    useEffect(() => {
        document.title = "Crop Recommendation || FarmTech";
    }, []);

    //posting data to server
    const postDataToServer = (data) => {
        axios.post(`${base_url}/agriData`, data).then(
            (response) => {
                console.log(response);
                console.log("success");
                showToastSuccessMessage();
                console.log(response.data.result);
                showResultToast(response);

            },
            (error) => {
                console.log(error);
                console.log("error");
                showToastErrorMessage();
            }
        );
    };

    return (
        <FormGroup className="form-group">
            <Typography variant="h4">Add Soil and Weather Details</Typography>
            <FormControl>
                <InputLabel>Name</InputLabel>
                <Input onChange={(e) => onValueChange(e)} name="name"/>
            </FormControl>

            <FormControl>
                <InputLabel>Country</InputLabel>
                <Select onChange={(e) => onValueChange(e)} name="country">
                    <MenuItem value={""}></MenuItem>
                    <MenuItem value={"Afghanistan"}>Afghanistan</MenuItem>
                    <MenuItem value={"Bangladesh"}>Bangladesh</MenuItem>
                    <MenuItem value={"Canada"}>Canada</MenuItem>
                    <MenuItem value={"China"}>China</MenuItem>
                    <MenuItem value={"India"}>India</MenuItem>
                    <MenuItem value={"Pakistan"}>Pakistan</MenuItem>
                    <MenuItem value={"USA"}>USA</MenuItem>
                    <MenuItem value={"Other"}>Other</MenuItem>
                </Select>
            </FormControl>

            <FormControl>
                <InputLabel>Adhaar Id</InputLabel>
                <Input onChange={(e) => onValueChange(e)} name="adhaarId"/>
            </FormControl>

            <FormControl>
                <InputLabel>Nitrogen</InputLabel>
                <Input onChange={(e) => onValueChange(e)} name="nitrogen"/>
            </FormControl>

            <FormControl>
                <InputLabel>Phosphorus</InputLabel>
                <Input onChange={(e) => onValueChange(e)} name="phosphorus"/>
            </FormControl>

            <FormControl>
                <InputLabel>Potassium</InputLabel>
                <Input onChange={(e) => onValueChange(e)} name="potassium"/>
            </FormControl>

            <FormControl>
                <InputLabel>Temperature</InputLabel>
                <Input onChange={(e) => onValueChange(e)} name="temperature"/>
            </FormControl>

            <FormControl>
                <InputLabel>Humidity</InputLabel>
                <Input onChange={(e) => onValueChange(e)} name="humidity"/>
            </FormControl>

            <FormControl>
                <InputLabel>Rainfall</InputLabel>
                <Input onChange={(e) => onValueChange(e)} name="rainfall"/>
            </FormControl>

            <FormControl>
                <InputLabel>PH</InputLabel>
                <Input onChange={(e) => onValueChange(e)} name="pH"/>
            </FormControl>
            <Button
                type="submit"
                variant="contained"
                color="primary"
                onClick={submitData}
            >
                Submit
                <ToastContainer/>
            </Button>
        </FormGroup>
    );
}
