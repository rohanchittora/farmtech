import React from "react";
import {Card} from "@mui/material";
import {CardContent} from "@mui/material";
import {CardMedia} from "@mui/material";
import {Typography} from "@mui/material";
import {Link} from "react-router-dom";
import "./CardItem.css";

function CardItem() {
    return (<div className="background">
      <span className="spanning-cards">
        <div className="empty-space3"></div>
<Link
    to="/services/crop-recommendation"
    className="crop-recommendation"
>
          <Card className="card-item">

            <CardMedia
                component="img"
                alt="Crop Recommendation"
                image="crop-image.jpg"
                title="Crop Recommendation"
                width="400px"
                height="350px"
            />
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                Crop Recommendation
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                Know best crop suitable for your agricultural field.
              </Typography>
            </CardContent>
          </Card>
</Link>
        <div className="empty-space4"></div>
        <Link
            to="/services/irrigation-requirement"
            className="irrigation-requirement"
        >
          <Card className="card-item2">
            <CardMedia
                component="img"
                alt="Irrigation Requirement"
                image="irrigation.jpg"
                title="Irrigation Requirement"
                width="400px"
                height="350px"
            />
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                Irrigation Requirement
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                Know if your crop required irrigation in real-time.
              </Typography>
            </CardContent>
          </Card>
        </Link>
        <div className="empty-space5"></div>
      </span>
        <div className="empty-space2"></div>
        <span className="spanning-cards">
        <div className="empty-space3"></div>
        <Link
            to="/services/fertilizer-recommendation"
            className="fertilizer-recommendation"
        >
          <Card className="card-item">
            <CardMedia
                component="img"
                alt="Fertilizer Recommendation"
                image="fertilizer.jpg"
                title="Fertilizer Recommendation"
                width="400px"
                height="350px"
            />
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                Fertilizer Recommendation
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                Know most suitable fertilizer for your crop.
              </Typography>
            </CardContent>
          </Card>
        </Link>
        <div className="empty-space4"></div>
        <Link
            to="/services/crop-disease-detection"
            className="crop-disease-detection"
        >
          <Card className="card-item2">
            <CardMedia
                component="img"
                alt="Crop Disease Detection"
                image="crop disease.jpg"
                title="Crop Disease Detection"
                width="400px"
                height="350px"
            />
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                Crop Disease Detection
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                Know if your crop suffers from any disease.
              </Typography>
            </CardContent>
          </Card>
        </Link>
        <div className="empty-space5"></div>
      </span>
        <div className="empty-space2"></div>
    </div>);
}

export default CardItem;
